#ifndef PATHVALUE_H
#define PATHVALUE_H

struct TreeNode {
  int data;
  struct TreeNode *left;
  struct TreeNode *right;
};

void btree_init(struct TreeNode **root);
void print_path(int path[], int top);
void find_path(struct TreeNode *root, int sum, int path[], int top);
void print_paths(struct TreeNode *root, int sum);

#endif
