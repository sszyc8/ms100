/* 腾讯面试题：
 * 给你 10 分钟时间，根据上排给出十个数，在其下排填出对应的十个数
 * 要求下排每个数都是先前上排那十个数在下排出现的次数。
 * 上排的十个数如下：
 * 【 0，1，2，3，4，5，6，7，8，9 】
 *
 * 举一个例子，
 * 数值: 0,1,2,3,4,5,6,7,8,9
 * 分配: 6,2,1,0,0,0,1,0,0,0
 * 0 在下排出现了 6 次，1 在下排出现了 2 次，
 * 2 在下排出现了 1 次，3 在下排出现了 0 次....
 * 以此类推..
 */
#include <iostream>
using namespace std;

int const len = 10;

class NumTB {
public:
  NumTB();
  int *getBottom();
  void setNextBottom();
  int getFrequency(int num);
  void printBottom();
private:
  int top[len];
  int bottom[len];
  bool success;
};

NumTB::NumTB() {
  success = false;

  // format top
  for (int i = 0; i < len; i++)
    top[i] = i;
}

int* NumTB::getBottom() {
  while (!success)
    setNextBottom();

  return bottom;
}

void NumTB::setNextBottom() {
  bool reB = true;
  for (int i = 0; i < len; i++) {
    int freq = getFrequency(i);
    if (bottom[i] != freq) {
      reB = false;
      bottom[i] = freq;
    }
  }
  
  success = reB;
}
// 此处的 num 即上排的数 i
int NumTB::getFrequency(int const num) {
  int freq = 0;
  for (int i = 0; i < len; i++)
    if (num == bottom[i])
      freq++;

  return freq;
}

void NumTB::printBottom() {
  for(int i = 0; i < len; i++)
    cout << bottom[i] << " ";

  cout << endl;
}

int main() {
  NumTB ntb;
  ntb.getBottom();
  ntb.printBottom();

  return 0;
}
