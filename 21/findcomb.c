#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static void helper(int n, int m, int *a, int index);

void findcomb(int n, int m)
{
    if (n < 1 || m < 1) {
	printf("No match!\n");
	return;
    }

    int *a;
    a = (int *)malloc(n*sizeof(int));
    assert(a != NULL);

    if (n > m)
	helper(m, m, a, -1);
    else
	helper(n, m, a, -1);
}

static void helper(int n, int m, int *a, int index)
{
    if (0 == m && index != -1) { /* echo the combinations */
	for (int i = index; i >= 0; --i)
	    printf("%d ", a[i]);
	printf("\n");
	
	return;
    }
    
    if (m < 0 || n < 1 || ((0 == m && -1 == index)))
	return;

    a[++index] = n;
    if (n-1 > m-n)              /* include n */
	helper(m-n, m-n, a, index);
    else
	helper(n-1, m-n, a, index);
    
    helper(n-1, m, a, --index);	/* exclude n */
}

int main(int argc, char *argv[])
{
    int n, m;
    if (argc != 3) {
	n = 5;
	m = 8;
    } else {
	n = atoi(argv[1]);
	m = atoi(argv[2]);
    }

    findcomb(n, m);

    exit(0);
}
