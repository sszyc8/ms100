#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int helper(int a[], int s, int e)
{
    int i = e - 1;
    int j;
    if (e == s) return 1;
    while (a[e] < a[i] && i >= s)
	--i;
    if (i == e - 1 || !helper(a, i+1, e-1))
	return 0;
    j = i;
    while (a[e] > a[j] && j>=s)
	--j;

    if (j < s)
	return helper(a, s, i);
    else
	return 0;
}

int isPostorder(int a[], int n)
{
    assert(a != 0 && n > 0);
    return helper(a, 0, n-1);
}

int main(int argc, char *argv[])
{
    int i;
    int *a = NULL;
    if (argc < 2)
	exit(0);

    a = (int *)malloc(sizeof(int)*(argc - 1));
    assert(a != NULL);
    
    for (i = 0; i < argc - 1; ++i)
	a[i] = atoi(argv[i+1]);

    if (isPostorder(a, argc - 1))
	printf("Yes\n");
    else
	printf("No\n");

    free(a);
    return 0;
}
