#ifndef MINSTACK_H
#define MINSTACK_H

typedef struct MinStackElement {
  int data;
  int min;
} MinStackElement;

typedef struct MinStack {
  MinStackElement *data;
  int size;
  int top;
} MinStack;

void minStackInit(MinStack *stack, int maxSize);

void minStackDestroy(MinStack *stack);

void minStackPush(MinStack *stack, int data);

int minStackPop(MinStack *stack);

int minStackMin(MinStack *stack);

#endif
