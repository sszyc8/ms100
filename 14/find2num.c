#include <stdio.h>
#include <stdlib.h>

void find2num(int a[], int n, int dest)
{
    int i = 0, j = n - 1;
    int sum = a[i] + a[j];

    while (sum != dest && i < j) {
	if (sum < dest)
	    ++i;
	else
	    --j;
	sum = a[i] + a[j];
    }

    if (sum == dest)
	printf("%d = %d + %d\n", dest, a[i], a[j]);
    else
	printf("Not found match\n");
}

int main(int argc, char* argv[])
{
    int dest;
    int a[] = {1, 2, 4, 7, 11, 15};
    int n = sizeof(a)/sizeof(int);

    if (argc != 2)
	dest = 15;
    else
	dest = atoi(argv[1]);

    find2num(a, n, dest);

    exit(0);
}
