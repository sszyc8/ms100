#include <stdio.h>
#include "lcs.h"

int main(int argc, char *argv[]) {
  char *str1 = NULL;
  char *str2 = NULL;

  if (argc != 3) {
    str1 = "BDCABA";
    str2 = "ABCBDAB";
  } else {
    str1 = argv[1];
    str2 = argv[2];
  }
  
  int num = lcs(str1, str2);

  printf("\nnum = %d\n", num);
  
  return 0;
}
