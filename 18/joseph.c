#include <stdio.h>
#include <stdlib.h>

int joseph(int n, int m, int start)
{
    if (!(n > 0 && m > 0 && start >= 0))
	return -1;

    int fn = 0;
    for (int i = 2; i <= n; ++i)
	fn = (fn + m)%i;

    return (fn + start)%n;
}

int main(int argc, char *argv[])
{
    int n, m, start;

    if (argc > 2) {
	n = atoi(argv[1]);
	m = atoi(argv[2]);
	if (argc > 3)
	    start = atoi(argv[3]);
	else
	    start = 0;
    } else {
	n = 10;
	m = 3;
	start = 0;
    }

    printf("n = %d, m = %d, start = %d\n", n, m, start);
    printf("last = %d\n", joseph(n, m, start));

    exit(0);
}
